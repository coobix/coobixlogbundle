<?php

namespace Coobix\LogBundle\EventSuscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Coobix\AdminBundle\CoobixAdminEvents;
use Coobix\LogBundle\Model\LogManager;

class LogSuscriber implements EventSubscriberInterface
{
	/**
     * @var LogManager
     */
    private $logManager;

	/**
     * FlashSuscriber constructor.
     *
     * @param Session  $session
     */
	public function __construct(LogManager $logManager)
	{
		$this->logManager = $logManager;
	}

	public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return array(
           CoobixAdminEvents::RECORD_CREATED => array(
               array('onRecordCreated', 0),
           ),
           CoobixAdminEvents::RECORD_UPDATED => array(
               array('onRecordUpdated', 0),
           ),
           CoobixAdminEvents::RECORD_DELETED => array(
               array('onRecordDeleted', 0),
           ),
           /*
           CoobixAdminEvents::RECORD_NOT_CREATED => array(
               array('onRecordNotCreated', 0),
           ),
           CoobixAdminEvents::RECORD_UPDATED => array(
               array('onRecordUpdated', 0),
           ),
           CoobixAdminEvents::RECORD_NOT_UPDATED => array(
               array('onRecordNotUpdated', 0),
           ),
           CoobixAdminEvents::RECORD_DELETED => array(
               array('onRecordDeleted', 0),
           ),
           CoobixAdminEvents::RECORD_NOT_DELETED => array(
               array('onRecordNotDeleted', 0),
           ),
           */
        );
    }

    protected function getClassName($record)
    {
      $rf = new \ReflectionClass($record);
      return $rf->getShortName();
    }

    public function onRecordCreated($event) 
    {
        $entity = $event->getRecord(); 
       	$this->logManager->create('CREATE ' . $this->getClassName($entity) . ': ' . $entity . '. ID: ' . $entity->getId());
    }

    public function onRecordUpdated($event) 
    {
        $entity = $event->getRecord(); 
        $this->logManager->create('UPDATE ' . $this->getClassName($entity) . ': ' . $entity . '. ID: ' . $entity->getId());
    }

    public function onRecordDeleted($event) 
    {

       $entity = $event->getRecord(); 
        $this->logManager->create('DELETE ' . $this->getClassName($entity) . ': ' . $entity . '. ID: ' . $entity->getId());
    }
    /*
    public function onRecordNotCreated() 
    {
       	$this->session->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO CREAR VERIFIQUE EL FORMULARIO.');
    }

    

    public function onRecordNotUpdated() 
    {
       	$this->session->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO MODIFICAR VERIFIQUE EL FORMULARIO.');
    }

    

    public function onRecordNotDeleted() 
    {
       	$this->session->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO ELIMINAR DEBIDO A QUE TIENE DATOS ASOCIADOS.');
    }
   */
	
}