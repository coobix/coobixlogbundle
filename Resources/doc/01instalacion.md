Instalación de CoobixLogBundle
==================================

## Prerequisitos

## Instalación

1. Descargar CoobixLogBundle usando composer
2. Habilitar el bundle


### Paso 1: Descargar CoobixLogBundle usando composer

Agregar el repositorio del bundle en composer.json:

``` bash
"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:coobix/coobixlogbundle.git"
        }
    ]
```

Agregar el bundle con el siguiente comando:

``` bash
$ composer require coobix/log-bundle "dev-master"
```

Composer instalara el  bundle en el directorio `vendor/coobix`.

### Paso 2: Habilitar el bundle

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Coobix\LogBundle\CoobixLogBundle(),
    );
}
```

### Paso 3: Configurar el bundle

``` yaml
# app/config/services.yml
services:
    coobix.log:
        class: Coobix\LogBundle\Model\LogManager
        arguments: 
            - "@doctrine"
            - "@security.token_storage"
            - "@logger"
        tags:
            - { name: monolog.logger, channel: my_app }

/..

monolog:
    channels: [my_app]
    handlers:
        my_app:
            type:     stream
            path:     "%kernel.logs_dir%/my_app.log"
            channels: [my_app]
```

### Step 4: Actualizar Base de Datos 

``` bash
$ php bin/console doctrine:generate:entities Coobix
```
``` bash
$ php bin/console doctrine:schema:update --force
```

### Step 4: Instalar Assests 

``` bash
$ php bin/console assets:install web --symlink
```
``` bash
$ php bin/console cache:clear
```

### Paso 5: Agregar item en el menu (CON EL ADMIN BUNDLE INSTALADO)


``` twig
# /src/Acme/AdminBundle/Resources/views/Admin/sidebar.html.twig
{% if (is_granted('ROLE_SUPER_ADMIN'))  %}
    <li>
        <a href="{{ path('admin_list', {'class': 'log'}) }}">Logs</a>
    </li>
{% endif %}

```

### Paso 6: Agregar servicio de entidad Admin (CON EL ADMIN BUNDLE INSTALADO)

``` yaml
# En app/config/config.yml agregar
services:
    coobix.admin.log:
        class: Coobix\LogBundle\Admin\Entity\LogAdmin
        arguments:
            - Coobix\LogBundle\Entity\Log
```