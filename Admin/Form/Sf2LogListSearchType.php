<?php

namespace Coobix\LogBundle\Admin\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class Sf2LogListSearchType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('text', 'text', array(
                    'required' => false,
                    'label' => 'Texto',
                ))
                ->add('user', 'text', array(
                    'required' => false,
                    'label' => 'Usuario',
                ))
                
              

        //fields
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
                //'data_class' => 'Coobix\LogBundle\Entity\Log'
        ));
    }

    
    public function getName() {
        return 'list_search';
    }

}