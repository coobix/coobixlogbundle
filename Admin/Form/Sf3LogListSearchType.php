<?php

namespace Coobix\LogBundle\Admin\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class Sf3LogListSearchType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('text', TextType::class, array(
                    'required' => false,
                    'label' => 'Texto',
                ))
                ->add('user', TextType::class, array(
                    'required' => false,
                    'label' => 'Usuario',
                ))
                
              

        //fields
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
                //'data_class' => 'Coobix\LogBundle\Entity\Log'
        ));
    }

    
    public function getBlockPrefix() {
        return 'list_search';
    }

}
