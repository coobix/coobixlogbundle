<?php

namespace Coobix\LogBundle\Admin\Entity;

$symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
switch ($symfonyVersion[0]) {
    case '2':
        class LogAdmin extends Sf2LogAdmin
        {
        };
        break;
    case '3':
        class LogAdmin extends Sf3LogAdmin
        {
        };
        break;
    case '4':
        class LogAdmin extends Sf4LogAdmin
        {
        };
        break;
}