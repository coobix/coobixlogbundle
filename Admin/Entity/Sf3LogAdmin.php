<?php

namespace Coobix\LogBundle\Admin\Entity;

use Coobix\AdminBundle\Entity\Admin;
use Coobix\LogBundle\Admin\Form\Sf3LogListSearchType as ListSearchType;

class Sf3LogAdmin extends Admin {
    
    public function getNewTitle() {
        return "Crear nuevo Log";
    }
    
    public function getEditTitle() {
        return "Editar Log";
    }
        
    public function __construct($class) {
        $this->setListTemplate('CoobixLogBundle:Log:Admin/list.html.twig');
        $this->setShowTemplate('CoobixLogBundle:Log:Admin/show.html.twig');
        $this->setListSearchForm(ListSearchType::class);
        $this->setNewForm(null);
        $this->setEditForm(null);
        parent::__construct($class);
        
    }

}
