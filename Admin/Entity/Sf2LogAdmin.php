<?php

namespace Coobix\LogBundle\Admin\Entity;

use Coobix\AdminBundle\Entity\Admin;
use Coobix\LogBundle\Admin\Form\Sf2LogListSearchType as ListSearchType;

class Sf2LogAdmin extends Admin {
    
    public function getNewTitle() {
        return "Crear nuevo Log";
    }
    
    public function getEditTitle() {
        return "Editar Log";
    }
        
    public function __construct($class) {
        $this->setListSearchForm(new ListSearchType());
        parent::__construct($class);
        
    }

}