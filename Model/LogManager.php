<?php

namespace Coobix\LogBundle\Model;

use Coobix\LogBundle\Entity\Log;

class LogManager {

    private $doctrine;
    private $security;
    private $monolog;
    private $entityManager;

    public function __construct($doctrine, $security, $monolog, $entityManager = 'default') {
        $this->doctrine = $doctrine;
        $this->security = $security;
        $this->monolog = $monolog;
        $this->entityManager = $entityManager;
    }

    public function create($msg) {

        $em = $this->doctrine->getManager($this->entityManager);

        $log = new Log();
        $log->setText($msg);

        $user = null;
        $userMsj = 'Usuario: ## ID: @@';
        $token = $this->security->getToken();
        if (null !== $token && is_object($token->getUser())) {
            $user = $token->getUser();

            $userMsj = 'Usuario: ## ID: @@.';
            $userMsj = str_replace('##', $user->getUsername(), $userMsj);
            $userMsj = str_replace('@@', $user->getId(), $userMsj);
        }

        $log->setUser($userMsj);

        $em->persist($log);
        $em->flush();
        
        $logger = $this->monolog;
        $logger->info($log);

        return $msg;
    }

}
